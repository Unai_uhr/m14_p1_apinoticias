import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { NoticiaInterfaz } from '../interfaces/interfaces';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getNews(){
    return this.http.get<NoticiaInterfaz>(`https://newsapi.org/v2/top-headlines?language=es&apiKey=${environment.apiKey}`)
    //return this.http.get<NoticiaInterfaz>(`https://newsapi.org/v2/top-headlines?language=es&apiKey=b92526ad6adf40ceb76183938e331e94`)


  }

  getNewsByCategory(categoria:string){
    console.log(`https://newsapi.org/v2/top-headlines?language=es&category=${categoria}&apiKey=${environment.apiKey}`);
    //return this.http.get<NoticiaInterfaz>(`https://newsapi.org/v2/sources?category=${categoria}&apiKey=${environment.apiKey}`)
    return this.http.get<NoticiaInterfaz>(`https://newsapi.org/v2/top-headlines?language=es&category=${categoria}&apiKey=${environment.apiKey}`)
  }


}
