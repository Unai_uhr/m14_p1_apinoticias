import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { Article } from '../interfaces/interfaces';


@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  noticiasFavoritas: Article[]=[];

  constructor(private storage:Storage) {
    this.storage.create();
    this.cargarFavoritos();
   }

   guardarNoticia(noticia: Article){
     this.noticiasFavoritas.unshift(noticia);
     this.storage.set('favoritos', this.noticiasFavoritas);
   }

   async cargarFavoritos(){
      const favoritos = await this.storage.get('favoritos');
      if(favoritos){
        this.noticiasFavoritas=favoritos;
      }
   }

   async quitarNoticia(noticia: Article){
      this.noticiasFavoritas = this.noticiasFavoritas.filter(noti => noti.title !== noticia.title );
      this.storage.set('favoritos', this.noticiasFavoritas);
   }


}
