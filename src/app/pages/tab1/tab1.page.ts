import { Component } from '@angular/core';
import { Article, NoticiaInterfaz } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';
//import { NoticiaComponent } from '../../noticia/noticia.component';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  listaNoticias: Article[]=[];

  constructor(private data:DataService) {}

  ngOnInit():void{
    this.loadNoticias();
  }

  loadData(event){
    this.loadNoticias(event)
  }


  loadNoticias(event?){
    this.data.getNews().subscribe(
      resp =>{
        console.log('Article',resp);  
        /*if(resp.next === null){
          event.target.disabled = true;
          event.target.complete();
          return;
        } */
        this.listaNoticias.push(...resp.articles);
        console.log(this.listaNoticias); 
        if(event){
          event.target.complete();
        }    
      });
  }

}
