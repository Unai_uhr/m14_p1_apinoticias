import { Component } from '@angular/core';
import { Article } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  categoriasNoticias:string[] = ['business','entertainment','general','health','science','sports','technology'];
  categoriaSeleccionada:string='business';

  listaNoticias: Article[]=[];

  constructor(private data:DataService) {}

  ngOnInit():void{
    this.loadNoticias(this.categoriaSeleccionada);
  }

  loadData(event){
    this.loadNoticias(this.categoriaSeleccionada,event)
  }


  loadNoticias(categoria:string,event?){
    this.data.getNewsByCategory(categoria).subscribe(
      resp =>{
        console.log('Category',resp);  
        this.listaNoticias.push(...resp.articles
          );
        console.log(this.listaNoticias); 
        if(event){
          event.target.complete();
        }  
      });
  }

  cambioSegmentoCategoria(event){
    console.log(event.detail.value);
    this.categoriaSeleccionada=event.detail.value;
    this.listaNoticias=[];

    this.loadNoticias(this.categoriaSeleccionada);

  }

}
