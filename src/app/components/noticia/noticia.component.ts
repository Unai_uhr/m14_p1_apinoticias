import { Component, Input, OnInit } from '@angular/core';
import { Article } from 'src/app/interfaces/interfaces';
import { Plugins } from '@capacitor/core';
import { ActionSheetController } from '@ionic/angular';
import { DataLocalService } from 'src/app/services/data-local.service';


@Component({
  selector: 'app-noticia',
  templateUrl: './noticia.component.html',
  styleUrls: ['./noticia.component.scss'],
})
export class NoticiaComponent implements OnInit {

  @Input() noticia: Article;
  @Input() currentIndex: number;
  @Input() isFavorite: any;


  constructor(public actionSheetController: ActionSheetController, private localData: DataLocalService) { }

  ngOnInit() {}

  async lanzarMenu() {

    console.log("localdata:--->", this.localData.noticiasFavoritas.includes(this.noticia))
    
    if(this.localData.noticiasFavoritas.includes(this.noticia)){
      this.isFavorite=true;
    }else{
      this.isFavorite=false;
    }

    let actionSheet;
    if(!this.isFavorite){
      actionSheet = await this.actionSheetController.create({
        header: 'Acciones',
        cssClass: 'my-custom-class',
        buttons: [{
          text: 'Compartir',
          icon: 'share',
          handler: () => {
            console.log('Share clicked');
            this.shareNoticia(this.noticia);
          }
        },{
          text: 'Favoritos',
          icon: 'star',
          handler: () => {
            console.log('Favoritos clicked');
            this.localData.guardarNoticia(this.noticia);
          }
        },{
          text: 'Cancelar',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
      await actionSheet.present();
      const { role } = await actionSheet.onDidDismiss();
    }else{
      const actionSheet = await this.actionSheetController.create({
        header: 'Acciones',
        cssClass: 'my-custom-class',
        buttons: [{
          text: 'Compartir',
          icon: 'share',
          handler: () => {
            console.log('Share clicked');
            this.shareNoticia(this.noticia);
          }
        },{
          text: 'Eliminar de mis favoritos',
          icon: 'trash',
          handler: () => {
            console.log('Eliminar de favoritos clicked');
            this.localData.quitarNoticia(this.noticia);
          }
        },{
          text: 'Cancelar',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
      await actionSheet.present();
      const { role } = await actionSheet.onDidDismiss();
    }

  }

  async abrirNavegador(){

    const { Browser } = Plugins;

    const newsUrl = this.noticia.url;

    await Browser.open({ url: newsUrl });


  }

  async shareNoticia(noticia: Article){
    const { Share } = Plugins;
    let shareRet = await Share.share(noticia);
  }

}
